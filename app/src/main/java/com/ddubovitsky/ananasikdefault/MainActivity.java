package com.ddubovitsky.ananasikdefault;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Player player;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        player = findViewById(R.id.player);

        startUpdatingScene();
    }

    private void startUpdatingScene() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                player.invalidate();
                startUpdatingScene();
            }
        }, 1000/60);
    }

}
