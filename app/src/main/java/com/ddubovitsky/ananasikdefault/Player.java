package com.ddubovitsky.ananasikdefault;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ddubovitsky on 4/6/18.
 */

public class Player extends View {

    private static final int WIDTH = 200;
    private static final int SPEED = 3;

    private Paint playerPaint;

    private int xPos;

    private int dX = SPEED;

    public Player(Context context) {
        super(context);
        init();
    }

    public Player(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Player(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        playerPaint = new Paint();
        playerPaint.setStyle(Paint.Style.FILL);
        playerPaint.setAntiAlias(true);
        playerPaint.setColor(Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(xPos, 200, xPos + WIDTH, 600, playerPaint);
        xPos += dX;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            changeDirection();
            return true;
        }

        return false;
    }

    private void changeDirection() {
        if (dX > 0)
            dX = -SPEED;
        else
            dX = SPEED;
    }
}
